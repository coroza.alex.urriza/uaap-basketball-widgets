# UaapBasketballWidgets

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Build

Run `ng build --prod --output-hashing none --single-bundle true --bundle-styles false --extra-webpack-config webpack.extra.js` to build the project. This command requires ngx-build-plus(which is already in the package.json)

## Running the example production website

You might need to install http-server to serve the sample website in your local machine. Run `npm install -g http-server` to install. And then run `http-server` from the root directory and then open `http://127.0.0.1:8080/sample-usage.html` in your browser.
