export const environment = {
  production: true,
  iconPath: 'https://arinola.sgp1.cdn.digitaloceanspaces.com/widgets/icons/',
  imagePath: 'https://arinola.sgp1.cdn.digitaloceanspaces.com/widgets/images/'
};
