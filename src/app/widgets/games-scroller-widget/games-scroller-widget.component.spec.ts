import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesScrollerWidgetComponent } from './games-scroller-widget.component';

describe('GamesScrollerWidgetComponent', () => {
	let component: GamesScrollerWidgetComponent;
	let fixture: ComponentFixture<GamesScrollerWidgetComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GamesScrollerWidgetComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GamesScrollerWidgetComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
