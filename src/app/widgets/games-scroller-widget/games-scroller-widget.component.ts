import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GameService } from 'src/app/services/game.service';
import { Game } from 'src/app/models/Game';

@Component({
	selector: 'games-scroller-widget',
	templateUrl: './games-scroller-widget.component.html',
	styleUrls: ['./games-scroller-widget.component.scss']
})
export class GamesScrollerWidgetComponent implements OnInit {

	@Input() gameDetailsRedirectUrl: string;
	gamesForSelectedDate: Game[];
	gameDate: string; /* get value from url */
	environment = environment;
	
	
	constructor(
		private gameService: GameService
	) { }

	ngOnInit() {
		this.getUrlParams();
		this.loadGamesForSelectedDate(this.gameDate);
	}


	getUrlParams() {
		let url = new URL(window.location.href);
		this.gameDate = url.searchParams.get('date');
	}



	loadGamesForSelectedDate(gameDate: string) {
		this.gameService.getGamesByDate(gameDate).subscribe((games: Game[]) => {
			this.gamesForSelectedDate = games;
		});
	}


	redirectToGameDetails(game: Game) {
		let url = this.gameDetailsRedirectUrl+'?gameId='+game.id+'&date='+game.date
		console.log(this.gameDetailsRedirectUrl);
		window.open(url);
	}

}
