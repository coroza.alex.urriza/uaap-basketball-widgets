import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameFullDetailsWidgetComponent } from './game-full-details-widget.component';

describe('GameFullDetailsWidgetComponent', () => {
	let component: GameFullDetailsWidgetComponent;
	let fixture: ComponentFixture<GameFullDetailsWidgetComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameFullDetailsWidgetComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameFullDetailsWidgetComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
