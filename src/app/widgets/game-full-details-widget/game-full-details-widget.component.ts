import { Component, OnInit } from '@angular/core';

import { environment } from 'src/environments/environment';
import { Game } from 'src/app/models/Game';
import { Stats } from 'src/app/models/Stats';
import { Play } from 'src/app/models/Play';

import { GameService } from 'src/app/services/game.service';

@Component({
	selector: 'game-full-details-widget',
	templateUrl: './game-full-details-widget.component.html',
	styleUrls: ['./game-full-details-widget.component.scss']
})
export class GameFullDetailsWidgetComponent implements OnInit {

	environment = environment;
	gameId: string; /* get value from url */
	gameDate: string; /* get value from url */
	gamesForSelectedDate: Game[];
	selectedGame: Game;


	statsList: Stats[] = [
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'lebron.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'lebron.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
	];
	
	playByPlayData: Array<{
		period: string;
		plays: Play[]
	}> = [
		{
			period: 'Q1', plays: [
				{ period: 'Q1', time: '12:00', playDescription: 'Jump Ball Horford vs. Sabonis: Tip to Smart', team: '' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'away' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'away' },
				{ period: 'Q1', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
			]
		},
		{
			period: 'Q2', plays: [
				{ period: 'Q2', time: '12:00', playDescription: 'Jump Ball Horford vs. Sabonis: Tip to Smart', team: '' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'away' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'away' },
				{ period: 'Q2', time: '11:37', playDescription: 'Tatum 12 Turnaround Fadeaway (2 PTS)', team: 'home' },
			]
		}
	];
	
	
	constructor(
		private gameService: GameService
	) { }

	ngOnInit() {
		this.getUrlParams();
		this.loadGamesForSelectedDate(this.gameDate);
		this.loadSelectedGame(this.gameId);
	}
	
	
	getUrlParams() {
		let url = new URL(window.location.href);
		this.gameId = url.searchParams.get('gameId');
		this.gameDate = url.searchParams.get('date');
	}


	loadGamesForSelectedDate(gameDate: string) {
		this.gameService.getGamesByDate(gameDate).subscribe((games: Game[]) => {
			this.gamesForSelectedDate = games;
		});
	}


	
	loadSelectedGame(gameId: string) {
		this.gameService.getGameById(gameId).subscribe((game: Game) => {
			this.selectedGame = game;
		});
	}


	/**
	 * Sets this.gameId and URL's gameId
	 */
	changeGameId(gameId: string) {
		this.gameId = gameId;
		let urlOrigin = window.location.origin;
		window.history.replaceState(null, null, urlOrigin+'/?gameId='+this.gameId+'&date='+this.gameDate);
	}


	/**
	 * Sets this.gameDate and URL's gameDate
	 */
	changeGameDate(stringDate: string) {
		this.gameDate = stringDate;
		let urlOrigin = window.location.origin;
		window.history.replaceState(null, null, urlOrigin+'/?gameId='+this.gameId+'&date='+this.gameDate);
	}

}
