import { Component, OnInit } from '@angular/core';
import { Stats } from 'src/app/models/Stats';
import { Game } from 'src/app/models/Game';
import { environment } from 'src/environments/environment';
import { League } from 'src/app/models/League';
import { TeamStanding } from 'src/app/models/TeamStanding';

@Component({
	selector: 'app-test-widget',
	templateUrl: './test-widget.component.html',
	styleUrls: ['./test-widget.component.scss']
})
export class TestWidgetComponent implements OnInit {

	environment = environment;


	gameDetails: Game = {
		id: 'qwe213qwe',
		status: 'Final / OT',
		homeTeam: {
			name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'Bulls', photoUrl: this.environment.imagePath+'bulls.svg',
			standing: { win: 6, loss: 1, streak: 'W-3' },
			players: [
				{
					name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
					statsList: [
						{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
					]
				}
			],
			statsList: [
				{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
				{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
				{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
				{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
				{ period: 'Final', pts: 103, reb: 12, ast: 7, stl: 5 }
			]
		},
		awayTeam: {
			name: 'Los Angeles Lakers', abbreviation: 'LAL', nickname: 'Lakers', photoUrl: this.environment.imagePath+'lakers.svg',
			standing: { win: 5, loss: 2, streak: 'L-3' },
			players: [
				{
					name: 'Lebron James', shortName: 'L. James', shirtNumber: 23, position: 'PG', photoUrl: this.environment.imagePath+'lebron.png',
					statsList: [
						{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
					]
				}
			],
			statsList: [
				{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
				{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
				{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
				{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
				{ period: 'Final', pts: 102, reb: 12, ast: 7, stl: 5 }
			]
		},
		playerOfTheGame: {
			name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
			statsList: [
				{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
				{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
				{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
				{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
				{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
			]
		},
		highlights: [
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
			{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
		]
	};


	statsList: Stats[] = [
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'lebron.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'lebron.png' } },
		{ ast: 3, blk: 2, dreb: 1, fg3Pct: 21, fg3a: 12, fg3m: 32, fgPct: 43, fga: 12, fgm: 43, ftPct: 54, fta: 12, ftm: 1, min: '26:16', oreb: 1, pf: 2, pts: 22, reb: 3, stl: 1, turnover: 11, player: { name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png' } },
	];

	league: League = {
		name: 'UAAP',
		season: '2018-2019'
	};

	teamStandings: TeamStanding[] = [
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } },
		{ win: 1, loss: 1, streak: 'L1', team: { name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'BULLS', photoUrl: this.environment.imagePath+'bulls.svg' } }
	];
	
	
	constructor() { }

	ngOnInit() {
	}

}
