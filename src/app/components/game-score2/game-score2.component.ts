import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Game } from 'src/app/models/Game';
import { Stats } from 'src/app/models/Stats';

@Component({
	selector: 'game-score2',
	templateUrl: './game-score2.component.html',
	styleUrls: ['./game-score2.component.scss']
})
export class GameScore2Component implements OnInit {

	@Input() game: Game;
	@Input() selectedGameId: string;
	@Output() onSelected = new EventEmitter<string>();
	isActive: boolean = false;
	environment = environment;
	
	
	constructor() { }

	ngOnInit() {
	}


	filterStatsByPeriod(period: string, statsList: Stats[]): Stats {
		let selectedStats: Stats;
		
		statsList.forEach((stats) => {
			if(stats.period === period) {
				selectedStats = stats;
			}
		});

		return selectedStats;
	}


	selectGame() {
		this.isActive = true; /* temporary only */
		this.onSelected.emit(this.game.id);
	}

}
