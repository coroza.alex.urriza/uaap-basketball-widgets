import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScore2Component } from './game-score2.component';

describe('GameScore2Component', () => {
	let component: GameScore2Component;
	let fixture: ComponentFixture<GameScore2Component>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameScore2Component]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameScore2Component);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
