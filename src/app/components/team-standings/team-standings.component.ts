import { Component, OnInit, Input } from '@angular/core';
import { TeamStanding } from 'src/app/models/TeamStanding';
import { League } from 'src/app/models/League';

@Component({
	selector: 'team-standings',
	templateUrl: './team-standings.component.html',
	styleUrls: ['./team-standings.component.scss']
})
export class TeamStandingsComponent implements OnInit {

	@Input() teamStandings: TeamStanding[];
	@Input() league: League;
	
	
	constructor() { }

	ngOnInit() {
	}

}
