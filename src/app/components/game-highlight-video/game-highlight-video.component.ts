import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';

import { GameHighlightVideo } from 'src/app/models/GameHighlightVideo';

@Component({
	selector: 'game-highlight-video',
	templateUrl: './game-highlight-video.component.html',
	styleUrls: ['./game-highlight-video.component.scss']
})
export class GameHighlightVideoComponent implements OnInit {

	@Input() gameHighlightVideo: GameHighlightVideo;
	environment = environment;
	
	constructor() { }

	ngOnInit() {
	}

}
