import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameHighlightVideoComponent } from './game-highlight-video.component';

describe('GameHighlightVideoComponent', () => {
	let component: GameHighlightVideoComponent;
	let fixture: ComponentFixture<GameHighlightVideoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameHighlightVideoComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameHighlightVideoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
