import { Component, OnInit, Input } from '@angular/core';
import { Game } from 'src/app/models/Game';

@Component({
	selector: 'game-score-per-period',
	templateUrl: './game-score-per-period.component.html',
	styleUrls: ['./game-score-per-period.component.scss']
})
export class GameScorePerPeriodComponent implements OnInit {

	@Input() game: Game;
	
	
	constructor() { }

	ngOnInit() {
	}

}
