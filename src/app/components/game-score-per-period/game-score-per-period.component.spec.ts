import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScorePerPeriodComponent } from './game-score-per-period.component';

describe('GameScorePerPeriodComponent', () => {
	let component: GameScorePerPeriodComponent;
	let fixture: ComponentFixture<GameScorePerPeriodComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameScorePerPeriodComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameScorePerPeriodComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
