import { Component, OnInit, Input } from '@angular/core';
import { Game } from 'src/app/models/Game';
import { Stats } from 'src/app/models/Stats';

@Component({
	selector: 'game-score-header',
	templateUrl: './game-score-header.component.html',
	styleUrls: ['./game-score-header.component.scss']
})
export class GameScoreHeaderComponent implements OnInit {

	@Input() game: Game;
	
	constructor() { }

	ngOnInit() {
	}


	filterStatsByPeriod(period: string, statsList: Stats[]): Stats {
		let selectedStats: Stats;
		
		statsList.forEach((stats) => {
			if(stats.period === period) {
				selectedStats = stats;
			}
		});

		return selectedStats;
	}

}
