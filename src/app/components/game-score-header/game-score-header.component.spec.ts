import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScoreHeaderComponent } from './game-score-header.component';

describe('GameScoreHeaderComponent', () => {
	let component: GameScoreHeaderComponent;
	let fixture: ComponentFixture<GameScoreHeaderComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameScoreHeaderComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameScoreHeaderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
