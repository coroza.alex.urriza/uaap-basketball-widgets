import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';


@Component({
	selector: 'game-schedule-selector',
	templateUrl: './game-schedule-selector.component.html',
	styleUrls: ['./game-schedule-selector.component.scss']
})
export class GameScheduleSelectorComponent implements OnInit {

	@Input() gamesCount: number;
	@Input() stringDate: string;
	@Output() onGameScheduleChange = new EventEmitter<string>();
	gameDate: Date = new Date();
	ngbDatePickerModel: { year, month, day, };
	environment = environment;
	
	
	constructor() { }

	ngOnInit() {
		this.stringDateToDate();
	}


	
	/**
	 * Convert this.stringDate to javascript Date and assign to this.gameDate
	 */
	stringDateToDate() {
		let date = moment(this.stringDate).toDate();
		this.gameDate = date;
	}


	selectNextDate() {
		let nextDate = moment(this.gameDate).add(1, 'days');
		this.gameDate = nextDate.toDate();
		this.emitData();
	}
	
	
	selectPrevDate() {
		let prevDate = moment(this.gameDate).subtract(1, 'days');
		this.gameDate = prevDate.toDate();
		this.emitData();
	}


	emitData() {
		let stringDate = moment(this.gameDate).format('YYYY-MM-DD');
		this.onGameScheduleChange.emit(stringDate);
	}


	ngbDatepickerModelToDate(event: { year, month, day }) {
		this.gameDate = new Date(event.year, event.month-1, event.day);
	}
	
	

}
