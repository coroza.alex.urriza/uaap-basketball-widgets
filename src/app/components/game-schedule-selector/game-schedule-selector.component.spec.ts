import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScheduleSelectorComponent } from './game-schedule-selector.component';

describe('GameScheduleSelectorComponent', () => {
	let component: GameScheduleSelectorComponent;
	let fixture: ComponentFixture<GameScheduleSelectorComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GameScheduleSelectorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GameScheduleSelectorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
