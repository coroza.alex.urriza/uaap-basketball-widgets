import { Component, OnInit, Input } from '@angular/core';
import { Stats } from 'src/app/models/Stats';

@Component({
	selector: 'players-stats-table',
	templateUrl: './players-stats-table.component.html',
	styleUrls: ['./players-stats-table.component.scss']
})
export class PlayersStatsTableComponent implements OnInit {

	@Input() statsList: Stats[];
	selectedTeam: string = 'MEM';
	
	
	constructor() { }

	ngOnInit() {
	}

}
