import { Component, OnInit, Input } from '@angular/core';
import { Play } from 'src/app/models/Play';

@Component({
	selector: 'play-by-play',
	templateUrl: './play-by-play.component.html',
	styleUrls: ['./play-by-play.component.scss']
})
export class PlayByPlayComponent implements OnInit {

	@Input() playByPlayData: Array<{
		period: string;
		plays: Play[]
	}>;
	
	
	constructor() { }

	ngOnInit() {
	}

}
