import { Component, OnInit } from '@angular/core';

import { environment } from 'src/environments/environment';
import { StatsCategory } from 'src/app/models/StatsCategory';
import { Player } from 'src/app/models/Player';

@Component({
	selector: 'game-leaders',
	templateUrl: './game-leaders.component.html',
	styleUrls: ['./game-leaders.component.scss']
})
export class GameLeadersComponent implements OnInit {

	environment = environment;

	selectedStatsCategory: StatsCategory;
	statsCategories: StatsCategory[];
	players: Player[];


	constructor() { }

	ngOnInit() {
		this.statsCategories = [
			{ value: 'points', displayName: 'Points' },
			{ value: 'rebounds', displayName: 'Rebounds' },
			{ value: 'assists', displayName: 'Assists' }
		];
		
		this.selectedStatsCategory = this.statsCategories[0];

		this.players = [
			{ id: 'player-1', teamId: 'lakers-123', name: 'Rajon Rondo', shortName: 'R. Rondo', shirtNumber: 9, position: 'PG', photoUrl: this.environment.imagePath+'lebron.png' },
			{ id: 'player-1', teamId: 'lakers-123', name: 'Lebron James', shortName: 'L. James', shirtNumber: 23, position: 'PG', photoUrl: this.environment.imagePath+'jordan.png' },
			{ id: 'player-1', teamId: 'lakers-123', name: 'McGee Javale', shortName: 'J. McGee', shirtNumber: 7, position: 'C', photoUrl: this.environment.imagePath+'lebron.png' },
			{ id: 'player-1', teamId: 'lakers-123', name: 'Anthony Davis', shortName: 'A. Davis', shirtNumber: 6, position: 'PF', photoUrl: this.environment.imagePath+'jordan.png' }
		];
	}

}
