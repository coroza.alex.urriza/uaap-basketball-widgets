import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as moment from 'moment';

import { environment } from 'src/environments/environment';
import { Game } from 'src/app/models/Game';



@Injectable({
	providedIn: 'root'
})
export class GameService {

	environment = environment;
	sampleGames: Game[];
	

	constructor() {
		this.loadSampleGames();
	}



	/**
	 * Will get Game[] based on the date provided
	 * @param stringDate 
	 */
	getGamesByDate(stringDate: string): Observable<Game[]> {
		let games: Game[] = [];

		this.sampleGames.forEach((game: Game) => {
			let convertedDate = moment(stringDate).format('YYYY-M-D');
			let convertedGameDate = moment(game.date).format('YYYY-M-D');
			if(convertedDate == convertedGameDate) {
				games.push(game);
			}
		});
		
		return of(games);
	}



	/**
	 * Get a Game based on id provided
	 * Returns null if non found
	 * @param id this is the gameId
	 */
	getGameById(id: string): Observable<Game> {
		let selectedGame: Game;
		
		this.sampleGames.forEach((game: Game) => {
			if(game.id == id) {
				selectedGame = game;
			}
		});

		return of(selectedGame);
	}



	loadSampleGames() {
		this.sampleGames = [
			{
				id: 'game-1',
				date: '2019-08-27',
				status: 'Final / OT',
				homeTeam: {
					name: 'Chicago Bulls', abbreviation: 'CHI', nickname: 'Bulls', photoUrl: this.environment.imagePath+'bulls.svg',
					standing: { win: 6, loss: 1, streak: 'W-3' },
					players: [
						{
							name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 103, reb: 12, ast: 7, stl: 5 }
					]
				},
				awayTeam: {
					name: 'Los Angeles Lakers', abbreviation: 'LAL', nickname: 'Lakers', photoUrl: this.environment.imagePath+'lakers.svg',
					standing: { win: 5, loss: 2, streak: 'L-3' },
					players: [
						{
							name: 'Lebron James', shortName: 'L. James', shirtNumber: 23, position: 'PG', photoUrl: this.environment.imagePath+'lebron.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 102, reb: 12, ast: 7, stl: 5 }
					]
				},
				playerOfTheGame: {
					name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
					statsList: [
						{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
					]
				},
				highlights: [
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
				]
			},
			{
				id: 'game-2',
				date: '2019-08-27',
				status: 'Final / OT',
				homeTeam: {
					name: 'Memphis Grizzlies', abbreviation: 'MEM', nickname: 'Grizzlies', photoUrl: this.environment.imagePath+'bulls.svg',
					standing: { win: 6, loss: 1, streak: 'W-3' },
					players: [
						{
							name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 99, reb: 12, ast: 7, stl: 5 }
					]
				},
				awayTeam: {
					name: 'New Orleans Pelicans', abbreviation: 'NOP', nickname: 'Pelicans', photoUrl: this.environment.imagePath+'lakers.svg',
					standing: { win: 5, loss: 2, streak: 'L-3' },
					players: [
						{
							name: 'Lebron James', shortName: 'L. James', shirtNumber: 23, position: 'PG', photoUrl: this.environment.imagePath+'lebron.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 98, reb: 12, ast: 7, stl: 5 }
					]
				},
				playerOfTheGame: {
					name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
					statsList: [
						{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
					]
				},
				highlights: [
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
				]
			},
			{
				id: 'game-3',
				status: 'Final / OT',
				date: '2019-08-26',
				homeTeam: {
					name: 'Houston Rockets', abbreviation: 'HOU', nickname: 'Rockets', photoUrl: this.environment.imagePath+'bulls.svg',
					standing: { win: 6, loss: 1, streak: 'W-3' },
					players: [
						{
							name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 110, reb: 12, ast: 7, stl: 5 }
					]
				},
				awayTeam: {
					name: 'Golden State Warriors', abbreviation: 'GSW', nickname: 'Warriors', photoUrl: this.environment.imagePath+'lakers.svg',
					standing: { win: 5, loss: 2, streak: 'L-3' },
					players: [
						{
							name: 'Lebron James', shortName: 'L. James', shirtNumber: 23, position: 'PG', photoUrl: this.environment.imagePath+'lebron.png',
							statsList: [
								{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
								{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
								{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
								{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
								{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
							]
						}
					],
					statsList: [
						{ period: '1Q', pts: 25, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 14, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 19, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 38, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 112, reb: 12, ast: 7, stl: 5 }
					]
				},
				playerOfTheGame: {
					name: 'Michael Jordan', shortName: 'M. Jordan', shirtNumber: 23, position: 'SG', photoUrl: this.environment.imagePath+'jordan.png',
					statsList: [
						{ period: '1Q', pts: 9, reb: 12, ast: 7, stl: 5 },
						{ period: '2Q', pts: 3, reb: 12, ast: 7, stl: 5 },
						{ period: '3Q', pts: 11, reb: 12, ast: 7, stl: 5 },
						{ period: '4Q', pts: 4, reb: 12, ast: 7, stl: 5 },
						{ period: 'Final', pts: 27, reb: 12, ast: 7, stl: 5 }
					]
				},
				highlights: [
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
					{ title: '[PHI 79-94] Jones Running Dunk Shot: Made (12 PTS)', date: '2019-02-14 22:22:22', previewPhotoUrl: this.environment.imagePath+'sample-highlight.png' },
				]
			}
		];
	}
}
