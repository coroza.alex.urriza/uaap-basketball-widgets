import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { createCustomElement } from '@angular/elements';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbButtonsModule, NgbProgressbarModule, NgbDatepickerModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MomentModule } from 'ngx-moment';

import { GameDetailsComponent } from './components/game-details/game-details.component';
import { TeamStandingsComponent } from './components/team-standings/team-standings.component';
import { GameScore2Component } from './components/game-score2/game-score2.component';
import { GameLeadersComponent } from './components/game-leaders/game-leaders.component';
import { TeamStatsComponent } from './components/team-stats/team-stats.component';
import { GameScoreHeaderComponent } from './components/game-score-header/game-score-header.component';
import { GameScorePerPeriodComponent } from './components/game-score-per-period/game-score-per-period.component';
import { PlayersStatsTableComponent } from './components/players-stats-table/players-stats-table.component';
import { GameScheduleSelectorComponent } from './components/game-schedule-selector/game-schedule-selector.component';
import { GameHighlightVideoComponent } from './components/game-highlight-video/game-highlight-video.component';
import { GameScoreComponent } from './components/game-score/game-score.component';
import { PlayByPlayComponent } from './components/play-by-play/play-by-play.component';

import { GameFullDetailsWidgetComponent } from './widgets/game-full-details-widget/game-full-details-widget.component';
import { TestWidgetComponent } from './widgets/test-widget/test-widget.component';
import { GamesScrollerWidgetComponent } from './widgets/games-scroller-widget/games-scroller-widget.component';

@NgModule({
	declarations: [
		GameDetailsComponent,
		TeamStandingsComponent,
		GameScore2Component,
		GameLeadersComponent,
		TeamStatsComponent,
		GameScoreHeaderComponent,
		GameScorePerPeriodComponent,
		PlayersStatsTableComponent,
		GameScheduleSelectorComponent,
		GameFullDetailsWidgetComponent,
		GameHighlightVideoComponent,
		GameScoreComponent,
		TestWidgetComponent,
		PlayByPlayComponent,
		GamesScrollerWidgetComponent,
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpClientModule,
		FlexLayoutModule,
		NgbButtonsModule, NgbProgressbarModule, NgbDatepickerModule, NgbTabsetModule,
		AngularSvgIconModule,
		MomentModule
	],
	entryComponents: [
		GameFullDetailsWidgetComponent,
		TestWidgetComponent,
		GamesScrollerWidgetComponent
	]
})
export class AppModule {
	constructor(private injector: Injector) { }

	ngDoBootstrap() {
		const gameFullDetailsWidget = createCustomElement(GameFullDetailsWidgetComponent, { injector: this.injector });
		customElements.define('basketball-game-full-details-widget', gameFullDetailsWidget);

		const testWidget = createCustomElement(TestWidgetComponent, { injector: this.injector });
		customElements.define('basketball-test-widget', testWidget);

		const gamesScrollerWidget = createCustomElement(GamesScrollerWidgetComponent, { injector: this.injector });
		customElements.define('basketball-games-scroller-widget', gamesScrollerWidget);
	}
}
