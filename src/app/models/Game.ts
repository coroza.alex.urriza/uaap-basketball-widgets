import { Team } from './Team';
import { Player } from './Player';
import { GameHighlightVideo } from './GameHighlightVideo';


export interface Game {
	id?: string;
	date?: string;
	season?: string;
	status?: string;
	time?: string;
	homeTeam?: Team;
	awayTeam?: Team;
	playerOfTheGame?: Player;
	highlights?: GameHighlightVideo[];
}