export interface StatsCategory {
	id?: number;
	value: string;
	displayName: string;
}