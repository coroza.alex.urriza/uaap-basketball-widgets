import { Team } from './Team';

export interface TeamStanding {
	id?: string;
	win?: number;
	loss?: number;
	streak?: string;
	winPct?: number;

	teamId?: string;
	team?: Team;
}