import { Stats } from './Stats';
import { Player } from './Player';
import { TeamStanding } from './TeamStanding';


export interface Team {
	id?: string;
	name?: string;
	abbreviation?: string;
	nickname?: string;
	photoUrl?: string;

	standing?: TeamStanding;

	players?: Player[];
	
	statsList?: Stats[];
}