import { Stats } from './Stats';
import { Team } from './Team';


export interface Player {
	id?: string;
	name?: string;
	shortName?: string;
	shirtNumber?: number;
	position?: string;
	photoUrl?: string;
	
	statsList?: Stats[];

	teamId?: string;
	team?: Team;
}