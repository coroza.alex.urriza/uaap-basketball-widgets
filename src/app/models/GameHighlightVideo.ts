export interface GameHighlightVideo {
	id?: string;
	title?: string;
	date?: string;
	previewPhotoUrl?: string;
}