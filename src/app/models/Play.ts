import { Team } from './Team';

export interface Play {
	id?: string;
	period?: string;
	time?: string;
	playDescription?: string;
	team?: string;
	
	awayScore?: number;
	homeScore?: number;
}