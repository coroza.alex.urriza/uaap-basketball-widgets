import { Game } from './Game';
import { Player } from './Player';
import { Team } from './Team';


export interface Stats {
	id?: string;
	ast?: number;
	blk?: number;
	dreb?: number;
	fg3Pct?: number;
	fg3a?: number;
	fg3m?: number;
	fgPct?: number;
	fga?: number;
	fgm?: number;
	ftPct?: number;
	fta?: number;
	ftm?: number;
	min?: string;
	oreb?: number;
	pf?: number;
	pts?: number;
	reb?: number;
	stl?: number;
	turnover?: number;

	period?: string; /* maybe periods like 1Q or OT2 or Final, etc */

	gameId?: number;
	game?: Game;

	playerId?: number;
	player?: Player;

	teamId?: number;
	team?: Team;
}